# XilinxPowerSensor

Example usage of the XilinxPowerSensor in the ASTRON PowerSensor Library: https://gitlab.com/astron-misc/libpowersensor/-/tree/master/xilinx

Simplified example with basic makefile.

# Usage
- Install the libpowersensor library: https://gitlab.com/astron-misc/libpowersensor/-/blob/master/README.md
- Find the Xilinx sensor path on your system
    - `$find /sys/devices -name xmc_power`
    - e.g.`/sys/devices/pci0000:a0/0000:a0:03.1/0000:a1:00.0/xmc.m.19922944/xmc_power`
- Update the paths in `libpowersensor_setup.sh` to match your system including the variable `POWERSENSOR_DEVICE`
- Source `libpowersensor_setup.sh`
- Run `make`
- Run the executable `main`
- When running in a new session again source `libpowersensor_setup.sh` before running the executable
