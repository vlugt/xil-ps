// Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: GPL-3.0-or-later
// Adapted from original author: Stefano Corda

#include <iostream>
#include<iomanip>
#include<limits>
#include <unistd.h>

#if defined(ENABLE_POWERSENSOR)
#include <powersensor/XilinxPowerSensor.h>
#endif

void report(
    std::string name,
    double seconds,
    float gflops,
    float gbytes,
    unsigned long mvis)
{
    int w1 = 20;
    int w2 = 7;
    std::cout << std::setw(w1) << std::string(name) << ": ";
    std::cout << std::setprecision(2) << std::fixed;
    std::cout << std::setw(w2) << seconds * 1e3 << " ms";
    if (gflops != 0) {
        std::cout << ", " << std::setw(w2) << gflops / seconds << " GFlops/s";
    }
    if (gbytes != 0) {
        std::cout << ", " << std::setw(w2) << gbytes / seconds << " GB/s";
    }
    if (gflops != 0 && gbytes != 0) {
        float arithmetic_intensity = gflops / gbytes;
        std::cout << ", " << std::setw(w2) << arithmetic_intensity << " Flop/byte";
    }
    if (mvis != 0) {
        std::cout << ", " << std::setw(w2) << mvis / seconds << " MVis/s";
    }
}

void report(
    std::string name,
    double seconds,
    float gflops,
    float gbytes,
    unsigned long mvis,
    double joules)
{
    report(name, seconds, gflops, gbytes, mvis);
    int w2 = 7;
    if (joules != 0) {
        double watt = joules / seconds;
        double efficiency = gflops / joules;
        std::cout << ", " << std::setw(w2) << watt << " W";
        std::cout << ", " << std::setw(w2) << efficiency << " GFlops/W";
    }
}


int main(){

	std::cout << "start" << std::endl;
	
#if defined(ENABLE_POWERSENSOR)
	std::unique_ptr<powersensor::PowerSensor> 
		powersensor(powersensor::xilinx::XilinxPowerSensor::create(0));
	powersensor::State xilinx_start, xilinx_end;
#endif

#if defined(ENABLE_POWERSENSOR)
	xilinx_start = powersensor->read();
#endif

	sleep(4);

#if defined(ENABLE_POWERSENSOR)
	xilinx_end = powersensor->read();
	double seconds = powersensor->seconds(xilinx_start, xilinx_end);
	double joules = powersensor->Joules(xilinx_start, xilinx_end);
	report("Test sleep", seconds, 0, 0, 0, joules);
	std::cout << std::endl;
#endif

	
	std::cout << "end" << std::endl;
	
}
