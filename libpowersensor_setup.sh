export ENABLE_POWERSENSOR=1
export POWERSENSOR_INCLUDE_DIR=~/bin/libpowersensor/include
export POWERSENSOR_LIB_DIR=~/bin/libpowersensor/lib
export LIBRARY_PATH=$LIBRARY_PATH:~/bin/libpowersensor/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/bin/libpowersensor/lib
export POWERSENSOR_DEVICE="/sys/devices/pci0000:a0/0000:a0:03.1/0000:a1:00.0/xmc.m.19922944/xmc_power"
