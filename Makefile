# the compiler: gcc for C program, define as g++ for C++
CC = g++

# compiler flags:
CFLAGS  = -g -Wall 

# libraries
LDLIBS=-l powersensor

# includes
INCLUDES=-I~/bin/libpowersensor/include

# The build target
TARGET = main

all: $(TARGET)

$(TARGET): $(TARGET).cpp
	$(CC) $(CFLAGS) $(LDLIBS) -I$(POWERSENSOR_INCLUDE_DIR) -L$(POWERSENSOR_LIB_DIR) -DENABLE_POWERSENSOR -o $(TARGET) $(TARGET).cpp

clean:
	$(RM) $(TARGET)
